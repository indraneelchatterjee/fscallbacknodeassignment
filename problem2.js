let fs = require("fs");
let path = require("path");
//Reading File
let readFile = (textPath, cb) => {
  fs.readFile(textPath, "utf8", (error, text) => {
    if (error) {
      console.log(error);
      return;
    }
    console.log("Lipsum.txt read.");
    return cb(text);
  });
};

let upperCase = (text, cb) => {
  let upperCaseText = text.toUpperCase();
  fs.writeFile(path.join(__dirname, "/upperCase.txt"), upperCaseText, (err) => {
    if (err) throw err;
  });
  fs.writeFile(
    path.join(__dirname, "/filenames.txt"),
    "upperCase.txt",
    (err) => {
      if (err) throw err;
    }
  );
  console.log("upperCase.txt Created");
  let upperCasePath = path.join(__dirname, "/upperCase.txt");
  return cb(upperCasePath);
};

//Lower Case & splitting
let lowerCaseAndSplit = (upperCasePath, cb) => {
  fs.readFile(upperCasePath, "utf8", (error, text) => {
    if (error) {
      throw error;
      return;
    }
    let lowerCaseText = text.toLowerCase().toString().split(".").join(".");
    fs.writeFile(
      path.join(__dirname, "/lowerCasedSplitted.txt"),
      lowerCaseText,
      (err) => {
        if (err) throw err;
      }
    );
    fs.appendFile(
      path.join(__dirname, "/filenames.txt"),
      "\nlowerCasedSplitted.txt",
      (err) => {
        if (err) throw err;
      }
    );
    console.log("lowerCasedSplitted.txt Created");
    let lowerCasedSplittedPath = path.join(
      __dirname,
      "/lowerCasedSplitted.txt"
    );
    cb(lowerCasedSplittedPath);
  });
};

//sorting
let sorting = (lowerCasedSplittedPath, cb) => {
  fs.readFile(lowerCasedSplittedPath, "utf8", (error, text) => {
    if (error) {
      console.log(error);
      return;
    }
    let sortedText = text.toString().split(".").sort().join(".");
    fs.writeFile(path.join(__dirname, "/sortedText.txt"), sortedText, (err) => {
      if (err) throw err;
    });
    fs.appendFile(
      path.join(__dirname, "/filenames.txt"),
      "\nsortedText.txt",
      (err) => {
        if (err) throw err;
      }
    );
    console.log("sortedText.txt Created");
    let fileNamesPath = path.join(__dirname, "/filenames.txt");
    return cb(fileNamesPath);
  });
};

let readAndDelete = (fileNamesPath) => {
  fs.readFile(fileNamesPath, "utf8", (err, fileNames) => {
    if (err) {
    }
    fileNames.split("\n").map((fileName) => {
      fs.unlink(path.join(__dirname, `/${fileName}`), (err) => {
        if (err) {
          throw err;
        }
      });
      console.log(`Deleted ${fileName} successfully.`);
    });
  });
};

module.exports = {
  readFile,
  upperCase,
  lowerCaseAndSplit,
  sorting,
  readAndDelete,
};
