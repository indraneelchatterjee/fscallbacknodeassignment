let path = require("path");
let {
  readFile,
  upperCase,
  lowerCaseAndSplit,
  sorting,
  readAndDelete,
} = require("../problem2");

let textPath = path.join(__dirname, "../lipsum.txt");

readFile(textPath, (text) => {
  return upperCase(text, (UpperCasePath) => {
    return lowerCaseAndSplit(UpperCasePath, (lowerCasedSplittedPath) => {
      return sorting(lowerCasedSplittedPath, (fileNamesPath) => {
        return readAndDelete(fileNamesPath);
      });
    });
  });
});
