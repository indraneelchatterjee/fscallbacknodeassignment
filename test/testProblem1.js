const { createDir, createFiles, readAndDelete } = require("../problem1");

let dirName = "randomJSON";

createDir(dirName, (dirPath) => {
  return createFiles(dirPath, (dirPath) => {
    return readAndDelete(dirPath);
  });
});
