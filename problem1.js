let fs = require("fs");
let path = require("path");
let createDir = (dirName, cb) => {
  let dirPath = path.join(__dirname, `/${dirName}`);
  fs.stat(dirPath, (err) => {
    if (err) {
      //creating directory
      fs.mkdir(dirPath, (error) => {
        if (error) {
          return error;
        }
        cb(dirPath);
      });
    } else {
      cb(dirPath);
    }
  });
};
//creating dummy files
let createFiles = (dirPath, cb) => {
  let dummyFile = {
    Name: `User ${Math.floor(Math.random() * 1000)}`,
    Place: `Place ${Math.floor(Math.random() * 1000)}`,
    Occupation: `Occupation ${Math.floor(Math.random() * 1000)}`,
  };
  let dummyJSON = JSON.stringify(dummyFile);
  fs.writeFile(`${dirPath}/dummyFile1.json`, dummyJSON, (err) => {
    if (err) throw err;
    console.log(` Dummy file1  created & saved.`);
  });
  fs.writeFile(`${dirPath}/dummyFile2.json`, dummyJSON, (err) => {
    if (err) throw err;
    console.log(` Dummy file2  created & saved.`);
  });

  return cb(dirPath);
};

//Reading and deleting dummy files
let readAndDelete = (dirPath) => {
  fs.readdir(`${dirPath}`, (err, dummyFiles) => {
    if (err) {
      throw err;
      return;
    }
    dummyFiles.map((file) => {
      fs.unlink(`${dirPath}/${file}`, (err) => {
        if (err) {
          throw err;
        }
      });
      console.log(`"Deleted ${file} successfully."`);
    });
  });
};

module.exports = { createDir, createFiles, readAndDelete };
